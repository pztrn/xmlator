package main

import (
	".."
	"fmt"
)

func main() {
	fmt.Println("Starting example program...")

	d := xmlator.NewDecoder("./example-small-normal.xml", nil)
	ch := d.GetChannel()
	compCh := make(chan bool, 1)

	go func(dataChan chan *xmlator.Object, completionChan chan bool) {
		for {
			select {
			case data := <-dataChan:
				fmt.Printf("Received data: %+v\n", data)
				if data.Type == xmlator.ObjectTypeEOF || data.Type == xmlator.ObjectTypeCriticalError {
					completionChan <- true
					return
				}
			}
		}
	}(ch, compCh)

	d.Decode()

	<-compCh
	close(ch)
	close(compCh)
	fmt.Println("Program completed")
}
