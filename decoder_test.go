package xmlator

import (
	// stdlib
	"testing"

	// other
	"github.com/stretchr/testify/require"
)

func TestDecoderNewWithoutOptions(t *testing.T) {
	d := NewDecoder("notexistantpath", nil)
	require.NotNil(t, d)
	require.True(t, d.IsFilePathSet())
}

func TestDecoderNewWithOptions(t *testing.T) {
	o := &Options{}
	d := NewDecoder("notexistantpath", o)
	require.NotNil(t, d)
	require.True(t, d.IsFilePathSet())
	require.True(t, d.IsOptionsSet())
}

func TestDecoderDecodeSmallNormal(t *testing.T) {
	d := NewDecoder("./examples/example-small-normal.xml", nil)
	require.NotNil(t, d)
	require.True(t, d.IsFilePathSet())
	require.True(t, d.IsOptionsSet())

	decoderChan := d.GetChannel()
	expectedChan := make(chan *Object, 1024)
	require.IsType(t, expectedChan, decoderChan)

	completionChan := make(chan bool, 1)

	go func(c chan *Object, completed chan bool) {
		t.Log("Launched separate goroutine for results receiving")
		for {
			select {
			case data := <-c:
				t.Logf("%+v", data)
				if data.Type == ObjectTypeEOF || data.Type == ObjectTypeCriticalError {
					completed <- true
					return
				}
			}
		}
	}(decoderChan, completionChan)

	d.Decode()

	<-completionChan
	t.Log("Test completed")
}
