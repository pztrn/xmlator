package xmlator

import (
	// stdlib
	"bytes"
	"io"
)

func (d *Decoder) decodeAttribute(startingByte byte, buffer *bytes.Buffer) *Object {
	entity := &Object{Type: ObjectTypeAttribute, Name: string(startingByte)}

	for {
		rawByte, err := buffer.ReadByte()
		// We can reach the end of our buffer. Or encounter another
		// error.
		if err != nil && err == io.EOF {
			entity.Type = ObjectTypeEOF
			entity.IsCompleted = true
		} else if err != nil && err != io.EOF {
			entity.Type = ObjectTypeCriticalError
			entity.Error = "Failed to read byte from internal buffer"
			entity.UnderlyingError = err
			entity.IsCompleted = true
			break
		}

		// "=" is a key separator. We should read data only until it appears.
		if rawByte == '=' {
			quotesAppeared := 0
			for {
				moreRawByte, err1 := buffer.ReadByte()
				// We can reach the end of our buffer. Or encounter another
				// error.
				if err1 != nil && err1 == io.EOF {
					entity.Type = ObjectTypeEOF
					entity.IsCompleted = true
				} else if err1 != nil && err1 != io.EOF {
					entity.Type = ObjectTypeCriticalError
					entity.Error = "Failed to read byte from internal buffer"
					entity.UnderlyingError = err1
					entity.IsCompleted = true
					break
				}

				// Skip quotes.
				if moreRawByte == '"' {
					quotesAppeared++
					if quotesAppeared == 2 {
						entity.IsCompleted = true
						break
					}
					continue
				}

				//entity.Content = append(entity.Content, moreRawByte)
				entity.Content += string(moreRawByte)

			}
		}

		if entity.IsCompleted {
			break
		}

		// Add stringified byte to current object name.
		entity.Name += string(rawByte)
	}

	return entity
}
