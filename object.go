package xmlator

// Object represents data object that is sent by Decoder goroutine.
type Object struct {
	// Type represents oject type.
	Type ObjectType
	// Is object completed? In some malformed XMLs we might need to
	// read another line to complete an object.
	IsCompleted bool
	// Name represents object's name.
	Name string
	// Depth represents object's depth related to entity. E.g. for
	// attributes Depth will be entity's depth + 1, for child elements
	// Depth will be also entity's depth + 1.
	Depth int
	// Content represents object's content as string.
	Content string

	// Valid for errors.
	// Error is a XMLator error as string.
	Error string
	// UnderlyingError is an error returned by underlying library. E.g.
	// if I/O error will occur - UnderlyingError will contain whole error
	// object from e.g. bytes.Buffer.ReadLine.
	UnderlyingError error
}
