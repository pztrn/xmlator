package xmlator

import (
	// stdlib
	"bufio"
	"bytes"
	"io"
	"os"
)

var (
	// Global options that will be used if no decoder-specific options
	// was set.
	options *Options
)

func init() {
	options = &Options{
		DataChannel: make(chan *Object, 1024),
	}
}

// Decoder is a decoding structure that does all the job.
type Decoder struct {
	// File-related things.
	// Path to file.
	filePath string
	// Opened file descriptor.
	file *os.File
	// Buffer from which Decoder will actually read data.
	fileBuffer *bufio.Reader

	// Options that is used for decoding.
	options *Options
}

// NewDecoder creates new decoder and returns it to caller.
// If options was passed they'll be set as decoder-specific options.
// Decoder is file-specific. If you want to parse another file you
// have to call Decoder.Close() and create new Decoder instance.
func NewDecoder(filepath string, passedOptions *Options) *Decoder {
	d := &Decoder{}
	if filepath != "" {
		d.setFilePath(filepath)
	}

	if passedOptions != nil {
		d.setOptions(passedOptions)
	} else {
		d.setOptions(options)
	}

	return d
}

// Close closes file we read.
func (d *Decoder) Close() {
	d.file.Close()
}

// Decode starts decoding data in separate goroutine.
// You should already be ready to receive data when calling this function.
func (d *Decoder) Decode() {
	go d.decodeReal()
}

// Really starts decoding data.
func (d *Decoder) decodeReal() {
	// Open file if it wasn't opened previously.
	if d.file == nil {
		fileHandler, err := os.Open(d.filePath)
		if err != nil {
			errObj := &Object{
				Type:            ObjectTypeCriticalError,
				Error:           "Failed to open file '" + d.filePath + "' for reading",
				UnderlyingError: err,
				IsCompleted:     true,
			}
			d.options.DataChannel <- errObj
			return
		}
		d.file = fileHandler
	}

	// Create buffered reader if it wasn't created previously.
	if d.fileBuffer == nil {
		d.fileBuffer = bufio.NewReader(d.file)
	}

	readBytes := bytes.NewBuffer(nil)

	// Start parsing.
	// It is not so simple and straightforward process because every
	// byte we parse can be almost anything.

	// We're always starting with depth = 1
	depth := 1
	// Are entity was decoded previously?
	entityWasBefore := false
	for {
		rawByte, err2 := readBytes.ReadByte()

		// We can reach the end of our buffer. Or encounter another
		// error.
		if err2 != nil && err2 == io.EOF {
			// Try to read next line and update buffer.
			newLineBytes, err3 := d.fileBuffer.ReadBytes('\n')
			if err3 != nil && len(newLineBytes) == 0 {
				errObj := &Object{
					Type:            ObjectTypeCriticalError,
					Error:           "Unable to read line from file '" + d.filePath + "'",
					UnderlyingError: err3,
					IsCompleted:     true,
				}
				d.options.DataChannel <- errObj
				break
			}

			readBytes.Reset()
			readBytes.Write(newLineBytes)
			continue
		} else if err2 != nil && err2 != io.EOF {
			errObj := &Object{
				Type:            ObjectTypeCriticalError,
				Error:           "Failed to read byte from internal buffer",
				UnderlyingError: err2,
				IsCompleted:     true,
			}
			d.options.DataChannel <- errObj
			return
		}

		var obj *Object

		// Figuring out what this byte stands for.

		// < stands for the beginning of new entity. We should read
		// more bytes until space (" "), closing thing (">") appears.
		if rawByte == '<' {
			obj = d.decodeEntity(readBytes)
		} else if rawByte == '>' || rawByte == ' ' {
			// We do not need empty spaces between elements.
			continue
		}

		if entityWasBefore && rawByte != '<' && rawByte != '>' {
			// We probably have attribute here.
			obj = d.decodeAttribute(rawByte, readBytes)
		}

		// If we have completed current object - send it to channel.
		if obj != nil && obj.IsCompleted {
			// Calculate depth.
			// If we have entity start object - we should increase
			// depth for all next elements.
			if obj.Type == ObjectTypeEntityStart {
				obj.Depth = depth
				depth++
				// If we'll encounter DOCTYPE entity - we should
				// RESET depth, because it has no closing tag.
				if obj.Type == ObjectTypeDocType {
					obj.Depth = 1
					depth = 1
				}
				entityWasBefore = true
			} else if obj.Type == ObjectTypeEntityClose {
				// If we have entity close object - depth should be
				// descreased.
				depth--
				obj.Depth = depth
				entityWasBefore = false
			} else if obj.Type == ObjectTypeAttribute {
				obj.Depth = depth
			}

			// If we have critical error - terminate loop.
			if obj.Type == ObjectTypeCriticalError {
				d.options.DataChannel <- obj
				break
			}

			// If we have EOF - try to read more bytes, unless we'll
			// encounter another EOF.
			if obj.Type == ObjectTypeEOF {
				newLineBytes, err := d.fileBuffer.ReadBytes('\n')
				if err != nil && err != io.EOF {
					errObj := &Object{
						Type:            ObjectTypeCriticalError,
						Error:           "Unable to read line from file '" + d.filePath + "'",
						UnderlyingError: err,
						IsCompleted:     true,
					}
					d.options.DataChannel <- errObj
					break
				} else if err != nil && err == io.EOF {
					errObj := &Object{
						Type:            ObjectTypeEOF,
						Error:           "Reached end of file",
						UnderlyingError: err,
						IsCompleted:     true,
					}
					d.options.DataChannel <- errObj
					break
				}

				readBytes.Reset()
				readBytes.Write(newLineBytes)
			} else {
				d.options.DataChannel <- obj
			}
		}
	}
}

// GetChannel returns channel that will be used to exchange parsed data
// and errors. If decoder wasn't initialized with Options structure
// which contains field for channel - default created channel will be
// used.
func (d *Decoder) GetChannel() chan *Object {
	return d.options.DataChannel
}

// IsFilePathSet returns true if filepath for decoder was set and false
// if isn't.
func (d *Decoder) IsFilePathSet() bool {
	return d.filePath != ""
}

// IsOptionsSet returns true if decoder-specific options was set and
// false if wasn't.
func (d *Decoder) IsOptionsSet() bool {
	return d.options != nil
}

// Sets file path to parse.
func (d *Decoder) setFilePath(filePath string) {
	d.filePath = filePath
}

// SetOptions set decoder-specific options for decoder.
func (d *Decoder) setOptions(options *Options) {
	d.options = options
}
