package xmlator

const (
	// ObjectTypeUnknown says that passed object type is unknown. Should
	// not appear on channel, if so - write a bugreport, please!
	// It might appear if Object wasn't initialized properly.
	// Also this type is used in tests where goroutines are used to signal
	// these goroutines to stop.
	ObjectTypeUnknown ObjectType = 0
	// ObjectTypeEntityStart says that passed object is an entity thing
	// and we have encountered it's opening tag.
	ObjectTypeEntityStart ObjectType = 1
	// ObjectTypeAttribute says that passed object is an attribute
	// to previously sent entity.
	ObjectTypeAttribute ObjectType = 2
	// ObjectTypeEntityClose says that passed object is an entity thing
	// and we have encountered it's closing tag.
	ObjectTypeEntityClose ObjectType = 3
	// ObjectTypeXMLDefinition says that passed object is describing
	// XML definition line (one starts with '<?xml').
	ObjectTypeXMLDefinition ObjectType = 4
	// ObjectTypeDocType says that passed object contains DOCTYPE
	// tag contents.
	ObjectTypeDocType ObjectType = 5
	// ObjectTypeEntitySelfClosed says us that we have self-closed
	// ("<entity ... />") thing and we should not increase it's depth.
	ObjectTypeEntitySelfClosed ObjectType = 6

	// ObjectTypeEOF says that passed object is an End-Of-File event
	// and receiving algorithm may stop it's work.
	ObjectTypeEOF ObjectType = 997
	// ObjectTypeError says that passed object is an error. This error
	// not neccessarily critical.
	ObjectTypeError ObjectType = 998
	// ObjectTypeCriticalError says that passed object is an critical
	// error. XMLator will not continue to work after sending this
	// object.
	ObjectTypeCriticalError ObjectType = 999
)

// ObjectType represents object type.
type ObjectType int
