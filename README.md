# XMLator

XML parsing library for Golang aimed to be as fast as possible and not memory-hungry even on big XML files.

## The history

Idea of creating this library was appeared when I need to parse big XML files (200+ megabytes) and convert them to another format (like JSON). All what I've googled was using default XML decoder and returns back a struct and there was no depth level information or no possibility to implement it without digging into Go sources itself.

## How it works

This library is designed to be used as streamer, so it required to launch parsing process in separate goroutine.

Files are read line-by-line, each line is iterated after for things like element starting sign, attribute, etc.

XMLator does not require your XML file to be perfectly valid.

## Default options

By default XMLator will read file line-by-line.