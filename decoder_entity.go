package xmlator

import (
	// stdlib
	"bytes"
	"io"
	"strings"
)

func (d *Decoder) decodeEntity(buffer *bytes.Buffer) *Object {
	entity := &Object{Type: ObjectTypeEntityStart}

	for {
		moreRawByte, err3 := buffer.ReadByte()
		// We can reach the end of our buffer. Or encounter another
		// error.
		if err3 != nil && err3 == io.EOF {
			entity.Type = ObjectTypeEOF
			entity.IsCompleted = true
		} else if err3 != nil && err3 != io.EOF {
			entity.Type = ObjectTypeCriticalError
			entity.Error = "Failed to read byte from internal buffer"
			entity.UnderlyingError = err3
			entity.IsCompleted = true
			break
		}

		// If space (" ") appeared - return into main cycle.
		// This would also mean that this object is completed.
		if moreRawByte == ' ' {
			entity.IsCompleted = true
			break
		}

		// If "/" appeared - then we have closing tag appeared
		// ("</...>"). In that case we should read more bytes
		// and complete object name.
		if moreRawByte == '/' {
			entity.Type = ObjectTypeEntityClose
			for {
				anotherRawByte, err4 := buffer.ReadByte()
				if err4 != nil && err4 == io.EOF {
					entity.Type = ObjectTypeEOF
					entity.IsCompleted = true
					break
				} else if err4 != nil && err4 != io.EOF {
					entity.Type = ObjectTypeCriticalError
					entity.Error = "Failed to read byte from internal buffer"
					entity.UnderlyingError = err4
					entity.IsCompleted = true
					break
				}
				// If this is a self-contained entity - do
				// nothing.
				if anotherRawByte == '>' {
					entity.IsCompleted = true
					break
				}

				// Add stringified byte to current object name.
				entity.Name += string(anotherRawByte)
			}
		}

		if entity.IsCompleted {
			break
		}

		// Add stringified byte to current object name.
		entity.Name += string(moreRawByte)
	}

	// Before returning an entity we should do one simple check.
	if strings.ToLower(entity.Name) == "?xml" {
		entity.Type = ObjectTypeXMLDefinition
	}
	if strings.Contains(strings.ToLower(entity.Name), "doctype") {
		entity.Type = ObjectTypeDocType
	}

	return entity
}
