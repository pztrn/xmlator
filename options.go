package xmlator

// Options represents options structure that can be used to configure
// decoder. Options can be global (set once) or per-decoder. If both
// global options and per-decoder set - decode-specific will be used
// for parsing. If decoder-specific options structure has some field
// unset it won't be replaced with value from global options.
type Options struct {
	// DataChannel represents an already opened channel which is ready
	// to receive data.
	DataChannel chan *Object
}
